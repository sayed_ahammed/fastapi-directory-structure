import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

# Load .env environment variables
load_dotenv()

DB_CONNECTION = os.environ.get("MYSQL_DB_CONNECTION")
DB_USER = os.environ.get("MYSQL_DB_USERNAME")
DB_PASSWORD = os.environ.get("MYSQL_DB_PASSWORD")
DB_HOST = os.environ.get("MYSQL_DB_HOST")
DB_PORT = os.environ.get("MYSQL_DB_PORT")
DB_DATABASE = os.environ.get("MYSQL_DB_DATABASE")
DB_CHARSET = "utf8"

MYSQL_URL = f"{DB_CONNECTION}+pymysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}" \
            f"/{DB_DATABASE}?charset={DB_CHARSET}"
POOL_SIZE = 20
POOL_RECYCLE = 3600
POOL_TIMEOUT = 15
MAX_OVERFLOW = 2
CONNECT_TIMEOUT = 60


class MySQLDB:
    def __init__(self) -> None:
        self.connection_is_active = False
        self.engine = None

    def get_db_connection(self):
        if not self.connection_is_active:
            connect_args = {"connect_timeout": CONNECT_TIMEOUT}
            try:
                self.engine = create_engine(MYSQL_URL, pool_recycle=POOL_RECYCLE, pool_timeout=POOL_TIMEOUT,
                                            pool_size=POOL_SIZE, max_overflow=MAX_OVERFLOW, connect_args=connect_args)
                return self.engine
            except Exception as ex:
                print("Error connection to DB : ", ex)
        return self.engine

    def get_db_session(self, engine):
        try:
            Session = sessionmaker(bind=engine)
            session = Session()
            return session
        except Exception as ex:
            print("Error getting DB session : ", ex)
            return None
