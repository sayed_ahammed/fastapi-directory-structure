from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, INTEGER, String, TIMESTAMP, BIGINT, BOOLEAN, text


Base = declarative_base()


class User(Base):
    __tablename__ = "users"
    
    id = Column(INTEGER, primary_key=True)
    uuid = Column(String)
    name = Column(String)
    email = Column(String)

