# imports
from fastapi import FastAPI

from src.app.routers.api import router as api_router


# create fast api app
app = FastAPI()

# load api router
app.include_router(api_router)

