from fastapi import APIRouter

from src.app.common.responses.success import success_response
from src.app.config.database.mysql import MySQLDB
from src.app.models.users.entities.user import User

router = APIRouter()

database = MySQLDB()
engine = database.get_db_connection()


@router.get('/users')
def get_all_users():
    session = database.get_db_session(engine)
    data = session.query(User).all()
    message = "Users retrieved successfully"
    return success_response(data, 200, message, False)

