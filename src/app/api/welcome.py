import os

from fastapi import APIRouter

router = APIRouter()


@router.get('/')
def greet():
    return "Hello World"


@router.get('/dot-env')
def dotenv():
    return os.environ.get("DB_DATABASE")



