from fastapi import APIRouter

from src.app.api import welcome, users

router = APIRouter()


router.include_router(welcome.router)
router.include_router(users.router)
