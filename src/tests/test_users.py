from starlette.testclient import TestClient
from src.app.main import app

client = TestClient(app)


def test_users():
    response = client.get("/users")
    assert response.status_code == 200

