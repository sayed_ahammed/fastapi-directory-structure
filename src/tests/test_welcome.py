from starlette.testclient import TestClient
from src.app.main import app

client = TestClient(app)


def test_greet():
    response = client.get("/")
    assert response.status_code == 200

